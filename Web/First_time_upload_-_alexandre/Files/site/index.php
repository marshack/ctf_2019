<!DOCTYPE html>
<html>
<head>
	<title>CyberLab 1.0</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/scrolling-nav.css" rel="stylesheet">
</head>
<body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Esioc</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Accueil</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="bg-primary text-white">
      <div class="container text-center">
        <h1>ESIOC 62.430</h1>
        <p class="lead">Escadron des Systèmes d'Information Opérationnels et de Cyberdefense</p>
      </div>
    </header>

    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h1>Bienvenue sur le site d'upload 1.0 du CyberLab</h1><br>
            <?php
            $html = "";
            if (isset($_FILES['uploaded'])) 
              {
                $target_path = "/var/www/html/uploads/";
                $target_path = $target_path . basename( $_FILES['uploaded']['name']);
                $uploaded_name = $_FILES['uploaded']['name'];
                $uploaded_type = $_FILES['uploaded']['type'];
                $uploaded_size = $_FILES['uploaded']['size'];
                if (($uploaded_type === "image/png") && ($uploaded_size < 10000000))
                  {         
                    if(!move_uploaded_file($_FILES['uploaded']['tmp_name'], $target_path)) 
                      {
                        $html .= '<pre>';
                        $html .= 'Le fichier ne correspond pas au format demandé !';
                        $html .= '</pre>';
                      } 
                    else 
                      {
                        $html .= '<pre>';
                        $html .= "<a href='/uploads/" . $_FILES['uploaded']['name'] . "'>" . $_FILES['uploaded']['name'] . "</a><br>" . 'Bravo ! ton image a été upload';
                        $html .= '</pre>';
                      }
                  }
                else
                  {
                    $html .= '<pre>';
                    $html .= 'Ceci est tous sauf une image !';
                    $html .= '</pre>';
                  }
                echo $html;
              }
            ?>
            <form enctype="multipart/form-data" action="#" method="POST">
              Seule les images au format .png sont acceptés : 
              <br><br>
              <input name="uploaded" type="file"><br>
              <br>
              <input type="submit" name="Upload" value="Upload">
            </form>
          </div>
        </div>
      </div>
    </section>
  </body>
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; MARS@HACK 2019</p>
    </div>
  <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>
</html>