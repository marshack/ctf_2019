<?php
session_start();

$bdd = new PDO('mysql:host=localhost;dbname=chall', 'chall', 'chall');

if(isset($_POST['formconnexion']))
{
	$pseudoconnect = htmlspecialchars($_POST['pseudoconnexion']);
	$passwordconnect = hash('sha256', $_POST['passwordconnexion']);
	if(!empty($pseudoconnect) AND !empty($passwordconnect))
	{
		$requser = $bdd->prepare("SELECT * FROM users WHERE login = ? AND password = ?");
		$requser->execute(array($pseudoconnect, $passwordconnect));
		$userexist = $requser->rowCount();
		if($userexist == 1)
		{
			$userinfo = $requser->fetch();
			$_SESSION['id'] = $userinfo['id'];
			$_SESSION['pseudo'] = $userinfo['login'];
			$_SESSION['mail'] = $userinfo['mail'];
			header("Location: profile.php");
		}
		else
			$erreur = "Mauvais couple identifiant/mot de passe !";
	}
	else
		$erreur = "Tous les champs doivent etre completes !";
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Connexion</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    	<!-- Bootstrap core CSS -->
    	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    	<!-- Custom styles for this template -->
    	<link href="css/scrolling-nav.css" rel="stylesheet">
	</head>
	<body>
		<!-- Navigation -->
	    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
	      <div class="container">
	        <a class="navbar-brand js-scroll-trigger" href="#page-top">Esioc</a>
	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
	          <span class="navbar-toggler-icon"></span>
	        </button>
	        <div class="collapse navbar-collapse" id="navbarResponsive">
	          <ul class="navbar-nav ml-auto">
	            <li class="nav-item">
	              <a class="nav-link" href="index.php">Accueil</a>
	            </li>
	            <?php
	            	if(!isset($_SESSION['id']))
	            		echo '<li class="nav-item"><a class="nav-link" href="inscription.php">Inscription</a></li>'
	            ?>
	            <li class="nav-item">
	            	<a class="nav-link" href="connexion.php">Se connecter</a>
	            </li>
	            <?php
	            	if(isset($_SESSION['id']))
	            		echo '<li class="nav-item"><a class="nav-link" href="deconnexion.php">Se déconnecter</a></li><li class="nav-item"><a class="nav-link" href="profile.php">Mon profile</a></li>';
	            ?>
	            <li class="nav-item">
	            	<a class="nav-link" href="admin.php">Administration</a>
	            </li>
	          </ul>
	        </div>
	      </div>
	    </nav>

	    <header class="bg-primary text-white">
	      <div class="container text-center">
	        <h1>ESIOC 62.430</h1>
	        <p class="lead">Escadron des Systèmes d'Information Opérationnels et de Cyberdefense</p>
	      </div>
	    </header>

		<section id="about">
      		<div class="container">
        		<div class="row">
          			<div class="col-lg-8 mx-auto">
						<div align="center">
							<h2>Connexion</h2>
							<br/><br/>
							<form method="POST" action="">
								<label for="pseudoconnexion">Pseudo :</label>
								<input type="text" placeholder="Votre pseudo" name="pseudoconnexion" id="pseudoconnexion"/>
								<label for="passwordconnexion">Password :</label>
								<input type="password" placeholder="Votre password" name="passwordconnexion" id="passwordconnexion"/><br><br>
								<input class="btn btn-primary" type="submit" name="formconnexion" value="Je m'identifie"/>
							</form>
							<?php
							if(isset($erreur))
								echo '<font color="red">' . $erreur . "</font>;"
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</body>
	<!-- Footer -->
	<footer class="py-5 bg-dark">
	    <div class="container">
	      	<p class="m-0 text-center text-white">Copyright &copy; MARS@HACK 2019</p>
	    </div>
	<!-- /.container -->
	</footer>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Plugin JavaScript -->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<!-- Custom JavaScript for this theme -->
	<script src="js/scrolling-nav.js"></script>
</html>