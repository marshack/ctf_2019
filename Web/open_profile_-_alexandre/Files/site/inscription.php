<?php
session_start();

if(isset($_SESSION['id']))
	header('Location: index.php');

$bdd = new PDO('mysql:host=localhost;dbname=chall', 'chall', 'chall');

if(isset($_POST['forminscription']))
{
	if(!empty($_POST['pseudo']) AND !empty($_POST['password']) AND !empty($_POST['password2']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']))
	{
		$pseudo = htmlspecialchars($_POST['pseudo']);
		$mail = htmlspecialchars($_POST['mail']);
		$mail2 = htmlspecialchars($_POST['mail2']);
		$password = hash('sha256', $_POST['password']);
		$password2 = hash('sha256', $_POST['password2']);

		$pseudolength = strlen($pseudo);
		if($pseudolength <= 255)
		{
			$reqpseudo = $bdd->prepare("SELECT * FROM users WHERE login = ?");
			$reqpseudo->execute(array($pseudo));
			$pseudoexist = $reqpseudo->rowCount();
			if($pseudoexist == 0)
			{
				if($mail === $mail2)
				{
					if(filter_var($mail, FILTER_VALIDATE_EMAIL))
					{
						$reqmail = $bdd->prepare("SELECT * FROM users WHERE mail = ? ");
						$reqmail->execute(array($mail));
						$mailexist = $reqmail->rowCount();
						if($mailexist == 0)
						{
							if($password === $password2)
							{
								$insert_user = $bdd->prepare("INSERT INTO users(login, password, email) VALUES(?,?,?)");
								$insert_user->execute(array($pseudo, $password, $mail));
								$erreur = "Votre compte à bien été créé !";
								header('Location: connexion.php');
							}
							else
								$erreur = "Vos passwords ne correspondent pas !";
						}
						else
							$erreur = "Adresse mail déjà utiliser !";
					}
					else
						$erreur = "Votre adresse mail n'est pas valide !";
				}
				else
					$erreur = "Vos adresses mail ne correspondent pas !";
			}
			else
				$erreur = "Pseudo déjà pris !";
		}
		else
			$erreur = "Votre pseudo ne doit pas dépassé 255 caractères !";
	}
	else
	{
		$erreur = "Tous les champs doivent être complétés !";
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Inscription</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    	<meta name="description" content="">
    	<meta name="author" content="">

    	<!-- Bootstrap core CSS -->
    	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    	<!-- Custom styles for this template -->
    	<link href="css/scrolling-nav.css" rel="stylesheet">
	</head>
	<body>
		<!-- Navigation -->
	    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
	      <div class="container">
	        <a class="navbar-brand js-scroll-trigger" href="#page-top">Esioc</a>
	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
	          <span class="navbar-toggler-icon"></span>
	        </button>
	        <div class="collapse navbar-collapse" id="navbarResponsive">
	          <ul class="navbar-nav ml-auto">
	            <li class="nav-item">
	              <a class="nav-link" href="index.php">Accueil</a>
	            </li>
	            <?php
	            	if(!isset($_SESSION['id']))
	            		echo '<li class="nav-item"><a class="nav-link" href="inscription.php">Inscription</a></li><li class="nav-item"><a class="nav-link" href="connexion.php">Se connecter</a></li>'
	            ?>
	            <?php
	            	if(isset($_SESSION['id']))
	            		echo '<li class="nav-item"><a class="nav-link" href="deconnexion.php">Se déconnecter</a></li><li class="nav-item"><a class="nav-link" href="profile.php">Mon profile</a></li><li class="nav-item"><a class="nav-link" href="admin.php">Administration</a></li>';
	            ?>
	          </ul>
	        </div>
	      </div>
	    </nav>

		<header class="bg-primary text-white">
	      <div class="container text-center">
	        <h1>ESIOC 62.430</h1>
	        <p class="lead">Escadron des Systèmes d'Information Opérationnels et de Cyberdefense</p>
	      </div>
	    </header>

		<section id="about">
      		<div class="container">
        		<div class="row">
          			<div class="col-lg-8 mx-auto">
						<div align="center">
							<h2>S'inscrire</h2>
							<br/><br/>
							<form method="POST" action="">
								<table>
									<tr>
										<td>
											<label for="pseudo">Pseudo :</label>
										</td>
										<td>
											<input type="text" placeholder="Votre pseudo" name="pseudo" id="pseudo"/>
										</td>
									</tr>
									<tr>
										<td>
											<label for="password">Password :</label>
										</td>
										<td>
											<input type="password" placeholder="Votre password" name="password" id="password"/>
										</td>
									</tr>
									<tr>
										<td>
											<label for="password2">Confirmation du Password :</label>
										</td>
										<td>
											<input type="password" placeholder="Confirmez password" name="password2" id="password2"/>
										</td>
									</tr>
									<tr>
										<td>
											<label for="mail">Email :</label>
										</td>
										<td>
											<input type="email" placeholder="Votre email" name="mail" id="mail"/>
										</td>
									</tr>
									<tr>
										<td>
											<label for="mail2">Confirmation Email :</label>
										</td>
										<td>
											<input type="mail" placeholder="Confirmez votre email" name="mail2" id="mail2"/>
										</td>
									</tr>
								</table>
								<br><br><input class="btn btn-primary" type="submit" name="forminscription" value="Je m'inscris"/>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php
		if(isset($erreur))
			echo '<font color="red">' . $erreur . "</font>";
		?>
	</body>
	<!-- Footer -->
  	<footer class="py-5 bg-dark">
    	<div class="container">
      		<p class="m-0 text-center text-white">Copyright &copy; MARS@HACK 2019</p>
    	</div>
  	<!-- /.container -->
  	</footer>
	<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="js/scrolling-nav.js"></script>
</html>