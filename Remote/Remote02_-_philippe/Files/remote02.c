// fichier source :  remote02.c
// gcc -m64 -o remote02 remote02.c


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <fcntl.h>

int main()
  {
                time_t origine = time(NULL);
                time_t stop;   
                int nb01,nb02,nb03,nb04,nb05=0;
                long int seed=0; 
                char suite[20];
                int res;
                char *flag="MARS{Ps3udo_Random_Numb3r_G3n3rator}";
                FILE *fp;

         
                fp = fopen("/dev/urandom","r");
                seed = fgetc(fp); 

                // seed = seed % 256;
                // printf("seed %ld\n",seed);                 
                srand(seed);


                // srand(4);

                printf("Find the 6th number ?\n\n");
    
                int i;
                for (i=0;i<10;i++)
                     {
                     rand();
                     }
                
                nb01 = rand() % 1000;
                nb02 = rand() % 1000;
                nb03 = rand() % 1000;
                nb04 = rand() % 1000;
                nb05 = rand() % 1000;
                                
                printf ("%04d\t%04d\t%04d\t%04d\t%04d\t????\n",nb01,nb02,nb03,nb04,nb05);
                
                
                
                
                
                printf("\n");
                printf("> ");
    fflush(stdout);


                fgets(suite,5,stdin);
                char *pos;
                if ((pos=strchr(suite, '\n')) != NULL)
                        *pos = '\0'; 

                stop = time(NULL);
                if ((stop-origine) >= 10)
                   { 
                   printf("Too late !!!!\n");
                   return(0);
                   } 

                res = rand() % 1000;
                if (res == atoi(suite))
                   {
                   printf("\nCongratulations, The flag is %s\n\n",flag);
       fflush(stdout);
                   }
                else
                   {
                   printf("\n");
                   printf("Lost :(\n\n");
       fflush(stdout);
                   }
                    
                // printf ("Solutions : %d\n",res);


        return(0);
  }
