<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <title>String Solver</title>
</head>
<body>
<center>
<?php
session_start();

function genImg($text) {
        $draw = new ImagickDraw();

        $draw->setFontSize(42);

        $draw->annotation(5, 55, $text);

        $img = new Imagick();
        $img->newImage(250, 70, "white");

        $img->drawImage($draw);

        $img->borderImage('black', 1, 1);

        $img->setImageFormat('png');

        return '<img src="data:image/png;base64,' . base64_encode($img) . '">';
}

function genStr($length) {
        return substr(md5(microtime()),rand(0,26),$length);
}

if (isset($_POST['string'])) {
        $cTime = time();
        $timeDiff = intval($cTime) - intval($_SESSION['time']);

        if ($timeDiff < 2) {
                if ($_SESSION['str'] == $_POST['string']) {
                	if ($_SESSION['lvl'] < 3) {
	                    	$_SESSION['lvl'] = intval($_SESSION['lvl']) + 1;
	                } else if ($_SESSION['lvl'] == 3) {
	                        echo "\n<br>\n<h2>Le flag est : MARS{Scr1pt7h1s0rl34v3}</h2>\n<br>\n";
	                        $_SESSION['lvl'] = 1;
	                        exit();
	                }
                } else {
                        echo "<h3>You failed !</h3>\n<br><br>\n";
                        $_SESSION['lvl'] = 1;
                }
        } else {
                echo "<h3>Trop tard !</h3>\n<br><br>\n";
        }
}

if ($_SESSION['lvl'] < 1 || $_SESSION['lvl'] > 3) {
        $_SESSION['lvl'] = 1;
} if ($_SESSION['lvl'] == 1) {
        $str1 = genStr(2);
        $str2 = genStr(2);
} else if ($_SESSION['lvl'] == 2) {
        $str1 = genStr(5);
        $str2 = genStr(5);
} else if ($_SESSION['lvl'] == 3) {
        $str1 = genStr(10);
        $str2 = genStr(10);
}

echo genImg($str1) . " + " . genImg($str2);

$_SESSION['str'] = $str1 . $str2;
$_SESSION['time'] = time();
?>
<p>Tu as 1s pour envoyer</p>
<p>Level <?php echo $_SESSION['lvl']; ?>/3</p>

<form method="post"><input type="text" name="string" required> <button type="submit">Submit</button></form>
</center>
</body>
</html>
