# coding: utf-8
# POC pour Mars@Hack
# Alain 2018




listbinaire1 = []
listbinaire2 = []

# Génération de l'hexa à partir d'une tile

tile2 =  ['. 1 . . . . . .',
          '. 2 . 1 . . . .',
          '. 1 2 1 1 . . .',
          '. . . 2 . . . .',
          '. . . 3 . . . .',
          '. . . . . . . .',
          '. . . . . . . .',
          '. . . . . . . .']

tile3 =  ['. 1 2 1 2 . . .',
          '. . . . 1 . . .',
          '. 1 2 1 1 . . .',
          '. 1 . . . . . .',
          '. 1 1 1 1 . . .',
          '. . . . . . . .',
          '. . . . . . . .',
          '. . . . . . . .']

tile4 =  ['. . 1 . 1 . . .',
          '. . . 1 . . . .',
          '. . 1 . 1 . . .',
          '. . . . . . . .',
          '. 1 2 1 . 3 1 1',
          '. 3 . 1 . 3 . .',
          '. 2 2 2 . 1 2 3',
          '. . . . . . . .']

hexa1 = []
hexa2 = []
for t in range(0,8):
    for i in tile3[t]:
        for elem in list(i)[0]:
            if elem == '.':
                hexa1.append(0)
                hexa2.append(0)
            elif elem == '1':
                hexa1.append(1)
                hexa2.append(0)
            elif elem == '2':
                hexa1.append(0)
                hexa2.append(1)
            elif elem == '3':
                hexa1.append(1)
                hexa2.append(1)

    print hex(int(''.join(map(str, hexa1)),2)),
    print hex(int(''.join(map(str, hexa2)),2)),
    print hexa1,
    print hexa2
    hexa1[:] = []
    hexa2[:] = []

print '\n'
# Generation du tile à partir des données hexa

#hexString = bytearray(b'\x40\x10\x58\x00\x10\x00\x00\x00\x00\x40\x20\x10\x10\x00\x00\x00') # 4
#hexString = bytearray(b'\x50\x08\x58\x40\x78\x00\x00\x00\x28\x00\x20\x00\x00\x00\x00\x00') # 2
hexString = bytearray(b'\x28\x10\x28\x00\x57\x54\x05\x00\x00\x00\x00\x00\x24\x44\x73\x00') # XOC


premierBloc =  hexString[0:8]
secondBloc =  hexString[8:]

[x for x in hexString[0:8]]

for i in premierBloc:
    listbinaire1.append(bin(i)[2:].zfill(8))
for i in secondBloc:
    listbinaire2.append(bin(i)[2:].zfill(8))

print listbinaire1
print listbinaire2

for a in range(0,8):
    for i in range(0,8):
        if int(list(listbinaire1[a])[i]) == 0 and int(list(listbinaire2[a])[i]) == 0:
            print ' ',
        elif int(list(listbinaire1[a])[i]) == 0 and  int(list(listbinaire2[a])[i]) == 1:
            print 2,
        elif int(list(listbinaire1[a])[i]) == 1 and  int(list(listbinaire2[a])[i]) == 0:
            print 1,
        elif int(list(listbinaire1[a])[i]) == 1 and int(list(listbinaire2[a])[i]) == 1:
            print 3,
        else:
            print "erreur"
    print '\n'