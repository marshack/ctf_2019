; source : crackme02.asm    
; Assemble:  nasm -f elf64 -l crackme02.lst  crackme02.asm
; Link:      gcc -m64 -s -o crackme02  crackme02.o
; Run:       ./reverse02 <flag>

  
; fonctions de la libc qui doivent etre utilisees

extern strlen             ;
extern printf             ; the C function, to be called

section .rodata           ; Rodata section

section .data             ; Data section, variables initialisés

banner: db 10 
        db "   __  __                        _    _            _",10    
        db "  |  \/  |                 ____ | |  | |          | |",10   
        db "  | \  / | __ _ _ __ ___  / __ \| |__| | __ _  ___| | __",10
        db "  | |\/| |/ _` | '__/ __|/ / _` |  __  |/ _` |/ __| |/ /",10
        db "  | |  | | (_| | |  \__ \ | (_| | |  | | (_| | (__|   < ",10
        db "  |_|  |_|\__,_|_|  |___/\ \__,_|_|  |_|\__,_|\___|_|\_\",10
        db "                          \____/",10                         
        db 0

fmt3: db "%s", 10, 0           ; The printf format, "\n",'0'

help: db "Help :",10
      db "    crackme02 <password>",10,0 

msg1: db "   Congratulations !",10,0  ; C string needs 0

fmt1: db "%s", 10, 10, 0  ; The printf format, "\n",'0'


msg2: db "Failed", 0      ; C string needs 0
fmt2: db "%s", 10,10, 0        ; The printf format, "\n",'0'


default rel 
global main                    
section .text              ; Code section.
 
main:                      ; the standard gcc entry point
                           ; the program label for the entry point
push rbp                   ; set up stack frame, must be alligned
 

push rdi
push rsi
mov rsi,banner                 ; affiche la banner asciiart 
mov rdi,fmt3             
xor rax,rax                 
call [printf wrt ..got]        ; Appelle de la fonction C printf (syntaxe compatible PIE)


pop rsi
pop rdi

cmp rdi,$2                     ; rdi => argc ,  rsi : argv
je continu                

mov rsi,help                   ; affiche l aide 
mov rdi,fmt3                
xor rax,rax                 
call [printf wrt ..got]     ; Appelle de la fonction C printf (syntaxe compatible PIE)
jmp fin

continu:

; -------------    verifie l'existence d'un paramêtre sur la ligne de commande    -------------

cmp rdi,$2                  ; rdi => argc ,  rsi : argv
jne fin       

mov rsi,qword[rsi+8]        ; [rsi+8] pointeur sur argument 1

; calcul taille argument 1                              

mov rdi, rsi                ; pointeur sur argument 1
call [strlen wrt ..got]
cmp rax, 0x19
jne badpassword             ; le password ne fait pas 25 caractères , on quitte 


xor rbx,rbx                 ;  index a 0

; mov rcx,codagexor
boucle :
mov rax,rsi                 ;    rsi pointeur sur chaine saisi  
add rax,rbx                 ;    rbx => index
movzx rax, byte[rax]        ;    rax  pointeur sur chaine saisi


mov rcx,codagexor               
add rcx,rbx
mov dil,byte[rcx]           ;    1er caractère du flag 
xor al,dil                  ;    caractère chaine saisi xor codage xor 
mov rdi,rcx
inc rdi
cmp al,byte[rdi]            
jne badpassword

mov rcx,rax                 ;    contient le caractère suivant
inc rbx                     ;    incrémente l'indes
cmp rbx,0x19                ;    compare l'indes a 25
jne boucle  

goodpassword:

mov rdi,fmt1
mov rsi,msg1
xor rax,rax 
call [printf wrt ..got]     ; Appelle de la fonction C printf (syntaxe compatible PIE)
jmp fin

badpassword: 
 
mov rdi,fmt2
mov rsi,msg2
xor rax,rax                
call [printf wrt ..got]     ; Appelle de la fonction C printf (syntaxe appelle compatible PIE)

fin:
pop rbp                     ; Restaure la pile
mov rax,0                   ; Normal, pas d'erreur
ret                         ; Fin du programme

codagexor: db 0x47,0x0a,0x4b,0x19,0x4a,0x31,0x77,0x16,0x7b,0x14,0x61,0x12,0x4d,0x15,0x5a,0x08,0x57,0x64,0x0a,0x69,0x06,0x62,0x0b,0x65,0x02,0x7f,0x00

